package reverse_proxy

import (
	"context"
	"fmt"
	"gopkg.in/yaml.v2"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type ListenCfg struct {
	Address string
	Port    int
}

type HostCfg struct {
	Address string
	Port    int
}

type ServiceCfg struct {
	Name         string
	Domain       string
	LoadBalancer string
	Hosts        []HostCfg
}

type ProxyCfg struct {
	Listen   ListenCfg
	Services []ServiceCfg
}

type Service struct {
	name         string
	loadBalancer loadBalancer
	hosts        []*httputil.ReverseProxy
}

func parseProxyConfig(c io.Reader) (ProxyCfg, error) {
	p := struct {
		Proxy ProxyCfg
	}{}

	err := yaml.NewDecoder(c).Decode(&p)

	return p.Proxy, err
}

func NewProxyFromConfigFile(c io.Reader) (*Proxy, error) {
	cfg, err := parseProxyConfig(c)

	if err != nil {
		return nil, err
	}

	return NewProxyFromConfig(cfg)
}

func NewProxyFromConfig(cfg ProxyCfg) (*Proxy, error) {
	services := map[string]Service{}
	for _, serviceCfg := range cfg.Services {
		var hosts []*httputil.ReverseProxy
		for _, hostCfg := range serviceCfg.Hosts {
			reverseProxy, err := newSingleHostReverseProxy(hostCfg)
			if err != nil {
				return nil, err
			}

			hosts = append(hosts, reverseProxy)
		}

		loadBalancer, err := instantiateLoadBalancer(serviceCfg.LoadBalancer, len(hosts))
		if err != nil {
			return nil, err
		}

		services[serviceCfg.Domain] = Service{
			name:         serviceCfg.Name,
			loadBalancer: loadBalancer,
			hosts:        hosts,
		}
	}

	return &Proxy{
		listen:   cfg.Listen,
		services: services,
	}, nil
}

func instantiateLoadBalancer(name string, instances int) (loadBalancer, error) {
	switch name {
	case "round_robin":
		return NewRoundRobinLoadBalancer(instances), nil
	case "random", "":
		return NewRandomLoadBalancer(instances), nil
	default:
		return nil, fmt.Errorf("unexpected load balancer %s", name)
	}
}

func newSingleHostReverseProxy(h HostCfg) (*httputil.ReverseProxy, error) {
	// TODO support https?
	u, err := url.Parse(fmt.Sprintf("http://%s:%d", h.Address, h.Port))
	if err != nil {
		return nil, err
	}

	return httputil.NewSingleHostReverseProxy(u), nil
}

type Proxy struct {
	listen   ListenCfg
	services map[string]Service
}

func (p *Proxy) Run(ctx context.Context) error {
	listen := p.listen

	server := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", listen.Address, listen.Port),
		Handler: http.HandlerFunc(p.serve),
	}

	go func() {
		<-ctx.Done()
		err := server.Shutdown(context.Background())
		if err != nil {
			log.Printf("error on server shutdown: %s", err)
		}
	}()

	return server.ListenAndServe()
}

func (p *Proxy) serve(w http.ResponseWriter, req *http.Request) {
	service := p.services[req.Host]

	nextInstance := service.loadBalancer.NextInstance()

	service.hosts[nextInstance].ServeHTTP(w, req)
}
