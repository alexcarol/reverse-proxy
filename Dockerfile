# syntax=docker/dockerfile:1
FROM golang:1.17 AS builder
RUN mkdir /code
WORKDIR /code
COPY . .
RUN go build ./cmd/reverse-proxy

FROM ubuntu:latest
WORKDIR /root/
COPY --from=builder /code/reverse-proxy .
CMD ["./reverse-proxy"]
