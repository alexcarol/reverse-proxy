Reverse Proxy
=============
How to run:

## Locally
###Build
```
go build ./cmd/reverse-proxy
```
###Run
```
./reverse-proxy --config-file=<config_file_location>
```

### With sample_config.yaml:
```
./reverse-proxy --config-file=./sample_config.yaml
```

## On Docker
### Build
```
docker build . -t reverse-proxy
```
### Run
```
docker run -v <absolute_route_to_config_file>:/config/config.yaml reverse-proxy ./reverse-proxy --config-file=/config/config.yaml
```
### With sample_config.yaml:
```
docker run -v ${PWD}/sample_config.yaml:/config/config.yaml reverse-proxy ./reverse-proxy --config-file=/config/config.yaml
```

Given that we want to implement multiple load-balancing methods, I've added a new optional field to the service with load balancing method that defaults to random.

About the implementation:
I've chosen an implementation where the base package (reverse-proxy) is implemented in a library-style. This would allow reusability of the code if we want to embed a proxy in another go server, while allowing it to be called from the main declared in ./cmd/reverse-proxy.

For the main I've chosen to implement it based on the standard library, but for a more advanced tool, particularly if we want to implement subcommands, I might consider an abstraction like cobra.

I've tried to keep the code as simple as possible, I'm reading the config using go-yaml, and create a proxy that contains multiple services, each with their load balancing strategy. With the current approach it should be easy to add new load balancing strategies, but we might need to modify the interface (e.g. we might want to have the balancer take into account active connections or resource usage).

To simplify, I've assumed that the requests are HTTP, the provided configuration is valid, and that we don't care about unhealthy instances (though a healthcheck should probably be an addition for a production-ready balancer).

One thing to consider, is that the current implementation spans a goroutine for each request, which has a footprint of ~2 KB (meaning 64 GB of memory would hold 32M concurrent goroutines, which is still a decent number), which might become a bottleneck depending on how many requests we want to handle (this isn't an issue for most web servers, as usually the work performed in the server trumps that usage. If we see that becoming an issue, I'd suggest diving a bit deeper and benchmarking the current implementation against another one with a limited amount of goroutines (a low number per core) that handle multiple channels (though that would require diving deeper into request handling, and would make the code a lot more complex).
