package main

import (
	"context"
	"flag"
	"fmt"
	reverse_proxy "gitlab.com/alexcarol/reverse-proxy"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	configFile := flag.String("config-file", "", "route to config file")
	flag.Parse()

	if *configFile == "" {
		fmt.Println("Missing config-file flag, usage:")
		flag.PrintDefaults()
		os.Exit(1)
	}

	file, err := os.Open(*configFile)
	if err != nil {
		log.Fatalf("error opening config file: %s", err)
	}

	p, err := reverse_proxy.NewProxyFromConfigFile(file)
	if err != nil {
		log.Fatalf("error instantiating proxy: %s", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	defer func() {
		signal.Stop(c)
		cancel()
	}()
	go func() {
		select {
		case <-c:
			cancel()
		case <-ctx.Done():
		}
	}()

	log.Println("starting proxy...")
	err = p.Run(ctx)
	if err != nil {
		log.Fatalf("proxy stopped: %s", err)
	}
}
