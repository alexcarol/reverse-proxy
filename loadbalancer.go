package reverse_proxy

import "math/rand"

type loadBalancer interface {
	// NextInstance returns the next instance traffic needs to be directed
	NextInstance() int
}

func NewRandomLoadBalancer(instances int) *RandomLoadBalancer {
	return &RandomLoadBalancer{
		instances: instances,
	}
}

// RandomLoadBalancer is a load balancer that returns a random instance
type RandomLoadBalancer struct {
	instances int
}

func (r *RandomLoadBalancer) NextInstance() int {
	// pseudorandom should be good enough for our uses, as we are just using it to balance the requests
	return rand.Int() % r.instances
}

func NewRoundRobinLoadBalancer(instances int) *RoundRobinLoadBalancer {
	return &RoundRobinLoadBalancer{
		instances: instances,
	}
}

// RoundRobinLoadBalancer is a load balancer that returns a random instance
type RoundRobinLoadBalancer struct {
	instances    int
	nextInstance int
}

func (r *RoundRobinLoadBalancer) NextInstance() int {
	nextInstance := r.nextInstance
	r.nextInstance = (r.nextInstance + 1) % r.instances

	return nextInstance
}
