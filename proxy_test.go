package reverse_proxy

import (
	"context"
	"github.com/stretchr/testify/require"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	go http.ListenAndServe("127.0.0.1:9090", echoHandler("a"))
	go http.ListenAndServe("127.0.0.1:9091", echoHandler("b"))
	go http.ListenAndServe("127.0.0.1:9092", echoHandler("c"))
	go http.ListenAndServe("127.0.0.1:9093", echoHandler("d"))

	os.Exit(m.Run())
}

func TestProxyConfigParser(t *testing.T) {
	tests := []struct {
		name    string
		config  string
		want    ProxyCfg
		wantErr error
	}{
		// TODO add more cases for incorrect values (invalid ports or ips) and
		// incorrect format (missing or extra fields, invalid syntax...)
		{
			name: "correct_config",
			config: `
proxy:
  listen:
    address: "127.0.0.1"
    port: 8080
  services:
    - name: my-service
      domain: my-service.my-company.com
      hosts:
      - address: "10.0.0.1"
        port: 9090
      - address: "10.0.0.2"
        port: 9090`,
			want: ProxyCfg{
				Listen: ListenCfg{
					Address: "127.0.0.1",
					Port:    8080,
				},
				Services: []ServiceCfg{
					{
						Name:   "my-service",
						Domain: "my-service.my-company.com",
						Hosts: []HostCfg{
							{
								Address: "10.0.0.1",
								Port:    9090,
							},
							{
								Address: "10.0.0.2",
								Port:    9090,
							},
						},
					},
				},
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseProxyConfig(strings.NewReader(tt.config))

			require.EqualValues(t, tt.wantErr, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestServicesUseTheRightBalancer(t *testing.T) {
	proxy, err := NewProxyFromConfig(ProxyCfg{
		Listen: ListenCfg{
			Address: "127.0.0.1",
			Port:    8080,
		},
		Services: []ServiceCfg{
			{
				Name:   "my-service",
				Domain: "default_balancer",
				Hosts: []HostCfg{
					{
						Address: "127.0.0.1",
						Port:    9090,
					},
				},
			},
			{
				Name:         "your-service",
				Domain:       "random_balancer",
				LoadBalancer: "random",
				Hosts: []HostCfg{
					{
						Address: "127.0.0.1",
						Port:    9092,
					},
				},
			},
			{
				Name:         "their-service",
				Domain:       "round_robin_balancer",
				LoadBalancer: "round_robin",
				Hosts: []HostCfg{
					{
						Address: "127.0.0.1",
						Port:    9092,
					},
				},
			},
		},
	})
	require.NoError(t, err)
	require.IsType(t, &RandomLoadBalancer{}, proxy.services["default_balancer"].loadBalancer)
	require.IsType(t, &RandomLoadBalancer{}, proxy.services["random_balancer"].loadBalancer)
	require.IsType(t, &RoundRobinLoadBalancer{}, proxy.services["round_robin_balancer"].loadBalancer)
}

func TestProxyRedirectsAppropriately(t *testing.T) {
	proxy := createProxy(t)
	ctx, cancel := context.WithCancel(context.Background())
	go proxy.Run(ctx)
	defer cancel()

	expectedBody := "test_body"
	req, err := http.NewRequest("GET", "http://127.0.0.1:8080/testurl", strings.NewReader(expectedBody))
	require.NoError(t, err)

	req.Host = "my-service.my-company.com"
	req.Header.Set("Custom-Test-Header", "thisisatest")

	resp, err := http.DefaultClient.Do(req)

	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.StatusCode)
	actualBody, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)

	require.Equal(t, expectedBody, string(actualBody))

	instanceID := resp.Header.Get("Instance-Id")
	require.Contains(t, []string{"a", "b"}, instanceID)

	customTestHeader := resp.Header.Get("Calling-Header-Custom-Test-Header")
	require.Equal(t, "thisisatest", customTestHeader)

	require.Equal(t, "/testurl", resp.Header.Get("Calling-Path"))
}

type stubLoadBalancer struct {
	nextInstance int
}

func (s *stubLoadBalancer) NextInstance() int {
	return s.nextInstance
}

func TestProxyRedirectsRequestWhereDirectedByLoadBalancer(t *testing.T) {
	proxy := createProxy(t)
	lb := &stubLoadBalancer{
		nextInstance: 0,
	}
	service := proxy.services["my-service.my-company.com"]
	service.loadBalancer = lb
	proxy.services["my-service.my-company.com"] = service

	ctx, cancel := context.WithCancel(context.Background())
	go proxy.Run(ctx)
	defer cancel()

	require.Equal(t, "a", sendRequestAndGetInstanceId(t))
	lb.nextInstance = 1
	require.Equal(t, "b", sendRequestAndGetInstanceId(t))
	require.Equal(t, "b", sendRequestAndGetInstanceId(t))
	require.Equal(t, "b", sendRequestAndGetInstanceId(t))

	lb.nextInstance = 0
	require.Equal(t, "a", sendRequestAndGetInstanceId(t))
}

func sendRequestAndGetInstanceId(t *testing.T) string {
	req, err := http.NewRequest("GET", "http://127.0.0.1:8080/testurl", strings.NewReader("test_body"))
	require.NoError(t, err)
	req.Host = "my-service.my-company.com"
	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.StatusCode)

	return resp.Header.Get("Instance-Id")
}

func createProxy(t *testing.T) *Proxy {
	p, err := NewProxyFromConfig(ProxyCfg{
		Listen: ListenCfg{
			Address: "127.0.0.1",
			Port:    8080,
		},
		Services: []ServiceCfg{
			{
				Name:   "my-service",
				Domain: "my-service.my-company.com",
				Hosts: []HostCfg{
					{
						Address: "127.0.0.1",
						Port:    9090,
					},
					{
						Address: "127.0.0.1",
						Port:    9091,
					},
				},
			},
			{
				Name:   "your-service",
				Domain: "your-service.your-company.com",
				Hosts: []HostCfg{
					{
						Address: "127.0.0.1",
						Port:    9092,
					},
					{
						Address: "127.0.0.1",
						Port:    9093,
					},
				},
			},
		},
	})

	require.NoError(t, err)

	return p
}

func echoHandler(instanceID string) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		w.Header().Add("Instance-Id", instanceID)
		w.Header().Add("Calling-Path", req.URL.Path)
		for headerName, headerValues := range req.Header {
			for _, headerValue := range headerValues {
				w.Header().Add("Calling-Header-"+headerName, headerValue)
			}
		}

		_, err := io.Copy(w, req.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("error writing response: %s\n", err)
		}
	}
}

func TestProxyStopsWhenContextCanceled(t *testing.T) {
	proxy := createProxy(t)

	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan error)
	go func() {
		err := proxy.Run(ctx)
		c <- err
	}()

	cancel()

	select {
	case err := <-c:
		require.ErrorIs(t, err, http.ErrServerClosed)
	case <-time.After(time.Second):
		t.Log("timeout waiting for run to be canceled")
		t.Fail()
	}
}
