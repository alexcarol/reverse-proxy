package reverse_proxy

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestRandomLoadBalancer_NextInstance(t *testing.T) {
	load := &RandomLoadBalancer{
		instances: 10,
	}
	firstValue := load.NextInstance()
	for i := 0; i < 10; i++ {
		res := load.NextInstance()
		require.Contains(t, []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, res)

		if res != firstValue {
			return
		}
	}
	t.Log("Random load balancer returned the same value after 10 tries")
	t.Fail()
}

func TestRoundRobinLoadBalancer_NextInstance(t *testing.T) {
	instanceAmount := 5
	load := &RoundRobinLoadBalancer{
		instances: instanceAmount,
	}
	for i := 0; i < 10; i++ {
		for j := 0; j < instanceAmount; j++ {
			res := load.NextInstance()
			require.Equal(t, j, res)
		}
	}
}